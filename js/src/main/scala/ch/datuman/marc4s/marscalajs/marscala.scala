package ch.datuman.marc4s
package marscalajs

import scala.scalajs.js

object MarscalaJS {
  class Record(var ldr: String, val controlFields: js.Array[ControlField], val dataFields: js.Array[DataField]) extends js.Object
  trait Field extends js.Any {
    var tag: String
  }
  class ControlField(var tag: String, var data: String) extends js.Object with Field
  class DataField(var tag: String, var i1: String, var i2: String, val subfields: js.Array[Subfield]) extends js.Object with Field {
    def :+(sf: Subfield) = new DataField(tag, i1, i2, subfields :+ sf)
  }
  class Subfield(var code: String, var data: String) extends js.Object

  import marscala.Marscala
  import scala.scalajs.js.JSConverters._

  object Record {
    def toMarscala(r: Record, defaultSfCode: Char = ' ', defaultIndicatorValue: Char = ' ') =
      new Marscala.Record(
        r.ldr,
        r.controlFields.map(ControlField.toMarscala).toList,
        r.dataFields.map(DataField.toMarscala(_, defaultSfCode: Char, defaultIndicatorValue)).toList
      )
    def apply(r: Marscala.Record) =
      new Record(r.ldr, r.controlFields.map(ControlField.apply).toJSArray, r.dataFields.map(DataField.apply).toJSArray)
  }
  object ControlField {
    def toMarscala(cf: ControlField)     = new Marscala.ControlField(cf.tag, cf.data)
    def apply(cf: Marscala.ControlField) = new ControlField(cf.tag, cf.data)
  }
  object DataField {
    def toMarscala(df: DataField, defaultSfCode: Char = ' ', defaultIndicatorValue: Char = ' ') =
      new Marscala.DataField(
        df.tag,
        df.i1.headOption.getOrElse(defaultIndicatorValue),
        df.i2.headOption.getOrElse(defaultIndicatorValue),
        df.subfields.map(Subfield.toMarscala(_, defaultSfCode)).toList
      )
    def apply(df: Marscala.DataField) = new DataField(df.tag, df.i1.toString, df.i2.toString, df.subfields.map(Subfield.apply).toJSArray)
  }
  object Subfield {
    def toMarscala(sf: Subfield, defaultSfCode: Char = ' ') = new Marscala.Subfield(sf.code.headOption.getOrElse(defaultSfCode), sf.data)
    def apply(sf: Marscala.Subfield)                        = new Subfield(sf.code.toString, sf.data)
  }

}
trait MarscalaJS extends MARC {
  type Collection   = Seq[Record]
  type Record       = MarscalaJS.Record
  type Field        = MarscalaJS.Field
  type ControlField = MarscalaJS.ControlField
  type DataField    = MarscalaJS.DataField
  type Subfield     = MarscalaJS.Subfield
}

class MarscalaJSOps extends MarcOps[MarscalaJS] with MarcTypes[MarscalaJS] {
  import marcTypes._
  implicit val dataField    = (f: DataField) => (f: MarscalaJS.Field)
  implicit val controlField = (f: ControlField) => f

  implicit val collectionRecords = ((c: Collection) => c)
  implicit val recordControlFields = { (rec: Record) =>
    rec.controlFields.toSeq
  }
  implicit val recordDataFields   = ((_: Record).dataFields.toSeq)
  implicit val dataFieldSubfields = ((_: DataField).subfields.toSeq)

  val recordLeader        = ((_: Record).ldr)
  val fieldTag            = ((_: Field).tag)
  val controlFieldValue   = ((_: ControlField).data)
  val dataFieldIndicator1 = ((_: DataField).i1.headOption.getOrElse(' '))
  val dataFieldIndicator2 = ((_: DataField).i2.headOption.getOrElse(' '))
  val subfieldCode        = ((_: Subfield).code.headOption.getOrElse(' '))
  val subfieldValue       = ((_: Subfield).data)

}
object MarscalaJSOps extends MarscalaJSOps

//object MarscalaJSFormat {
//  import scala.scalajs.js.JSConverters._
//  import MarscalaJS._
//  import play.api.libs.json._
//  import play.api.libs.json.Reads._
//  import play.api.libs.functional.syntax._
//  implicit val subfieldFmt = (
//      (JsPath \ "code").format[String] and
//      (JsPath \ "data").format[String]
//    )(Subfield.apply _,Subfield.unapply _)
//  implicit val controlFieldFmt = (
//      (JsPath \ "tag").format[String] and
//      (JsPath \ "data").format[String]
//    )(ControlField.apply _,ControlField.unapply _)
//  val sfsFmt=Format(
//    Reads.ArrayReads[Subfield].map(a=>a.toJSArray),
//    Writes.arrayWrites[Subfield].contramap((a:js.Array[Subfield]) => a.toArray)
//  )
//  implicit val dataFieldFmt = (
//      (JsPath \ "tag").format[String] and
//      (JsPath \ "i1").format[String] and
//      (JsPath \ "i2").format[String] and
//      (JsPath \ "subfields").format(sfsFmt)
//    )(DataField.apply _,DataField.unapply _)
//  val cfsFmt=Format(
//    Reads.ArrayReads[ControlField].map(a=>a.toJSArray),
//    Writes.arrayWrites[ControlField].contramap((a:js.Array[ControlField]) => a.toArray)
//  )
//  val dfsFmt=Format(
//    Reads.ArrayReads[DataField].map(a=>a.toJSArray),
//    Writes.arrayWrites[DataField].contramap((a:js.Array[DataField]) => a.toArray)
//  )
//  implicit val recordFmt =  (
//      (JsPath \ "ldr").format[String] and
//      (JsPath \ "controlFields").format(cfsFmt) and
//      (JsPath \ "dataFields").format(dfsFmt)
//    )(Record.apply _,Record.unapply _)
//  implicit val collectionFmt = Format(Reads.seq(recordFmt),Writes.seq(recordFmt))
////  implicit val fieldFmt : Format[Field]=
////     Format(
////       (__ \ "cls").read[String].flatMap{
////          case cls:String if cls == classOf[ControlField].getName => controlFieldFmt.map(v=>v)
////          case cls:String if cls == classOf[DataField].getName => dataFieldFmt.map(v=>v)
////          case cls:String => Reads(_ => JsError("Invalid field cls: " + cls))
////       },
////       Writes(
////         (c:Field) => {
////           (c match {
////             case cf: ControlField => controlFieldFmt.writes(cf)
////             case df: DataField => dataFieldFmt.writes(df)
////           })
////           .as[JsObject] +
////           ("cls",JsString(c.getClass.getName))
////         }
////       )
////
////    )
////  implicit val cfsFmt = Format(Reads.seq(controlFieldFmt),Writes.seq(controlFieldFmt))
////  implicit val dfsFmt = Format(Reads.seq(dataFieldFmt),Writes.seq(dataFieldFmt))
////  implicit val recordFmt = Json.format[Record]
////  implicit val collectionFmt = Format(Reads.seq(recordFmt),Writes.seq(recordFmt))
//}
