package ch.datuman.marc4s
package marscala
package json

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import ch.datuman.marc4s.marscala.Marscala
import ch.datuman.marc4s.marscala.Marscala.ControlField
import ch.datuman.marc4s.marscala.Marscala.DataField
import scala.scalajs.js.annotation.JSExportAll

object MarscalaFormat {
  import Marscala._
  implicit val charFmt         = implicitly[Format[String]].inmap[Char](_.headOption.getOrElse(' '), _.toString)
  implicit val subfieldFmt     = Json.format[Subfield]
  implicit val controlFieldFmt = Json.format[ControlField]
  implicit val dataFieldFmt    = Json.format[DataField]
  implicit val fieldFmt: Format[Field] =
    Format(
      (__ \ "cls").read[String].flatMap {
        case cls: String if cls == classOf[ControlField].getName => controlFieldFmt.map(v => v)
        case cls: String if cls == classOf[DataField].getName    => dataFieldFmt.map(v => v)
        case cls: String                                         => Reads(_ => JsError("Invalid field cls: " + cls))
      },
      Writes(
        (c: Field) => {
          (c match {
            case cf: ControlField => controlFieldFmt.writes(cf)
            case df: DataField    => dataFieldFmt.writes(df)
          }).as[JsObject] +
            ("cls", JsString(c.getClass.getName))
        }
      )
    )
  implicit val cfsFmt        = Format(Reads.seq(controlFieldFmt), Writes.seq(controlFieldFmt))
  implicit val dfsFmt        = Format(Reads.seq(dataFieldFmt), Writes.seq(dataFieldFmt))
  implicit val recordFmt     = Json.format[Record]
  implicit val collectionFmt = Format(Reads.seq(recordFmt), Writes.seq(recordFmt))
}
