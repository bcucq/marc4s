package ch.datuman.marc4s
package marscala

import scala.scalajs.js.annotation.JSExportAll

object Marscala {
  @JSExportAll case class Record(ldr: String, controlFields: Seq[ControlField], dataFields: Seq[DataField])
  trait Field {
    val tag: String
  }
  @JSExportAll case class ControlField(tag: String, data: String) extends Field
  @JSExportAll case class DataField(tag: String, i1: Char, i2: Char, subfields: Seq[Subfield]) extends Field {
    def :+(sf: Subfield) = DataField(tag, i1, i2, subfields :+ sf)
  }
  @JSExportAll case class Subfield(code: Char, data: String)

}
trait Marscala extends MARC {
  type Collection   = Seq[Record]
  type Record       = Marscala.Record
  type Field        = Marscala.Field
  type ControlField = Marscala.ControlField
  type DataField    = Marscala.DataField
  type Subfield     = Marscala.Subfield
}

class MarscalaOps extends MarcOps[Marscala] with MarcTypes[Marscala] {
  import marcTypes._
  implicit val dataField    = (f: DataField) => (f: Marscala.Field)
  implicit val controlField = (f: ControlField) => f

  implicit val collectionRecords = ((c: Collection) => c)
  implicit val recordControlFields = { (rec: Record) =>
    rec.controlFields
  }
  implicit val recordDataFields   = ((_: Record).dataFields)
  implicit val dataFieldSubfields = ((_: DataField).subfields)

  val recordLeader        = ((_: Record).ldr)
  val fieldTag            = ((_: Field).tag)
  val controlFieldValue   = ((_: ControlField).data)
  val dataFieldIndicator1 = ((_: DataField).i1)
  val dataFieldIndicator2 = ((_: DataField).i2)
  val subfieldCode        = ((_: Subfield).code)
  val subfieldValue       = ((_: Subfield).data)

}
object MarscalaOps extends MarscalaOps

object MarscalaConstructorOps extends MarscalaConstructorOps
trait MarscalaConstructorOps extends MarcConstructorOps[Marscala] {
  val newRecord       = Marscala.Record.apply _ //{case (ldr,cfs,dfs) => Marscala.Record(ldr,cfs,dfs) }
  val newControlField = Marscala.ControlField.apply _
  val newDataField    = Marscala.DataField.apply _
  val newSubfield     = Marscala.Subfield.apply _
}
object FullMarscalaOps extends MarscalaOps with MarscalaConstructorOps

object MarscalaUtility extends util.MarcUtility()(FullMarscalaOps)
