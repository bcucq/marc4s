package ch.datuman.marc4s

object MARC {
//  sealed case class Property[-e,p](property:e=>p)
  trait Property[-e, p] {
    val property: e => p
  }
}

trait MARC {
  type Collection
  type Record
  type Field
  type ControlField
  type DataField
  type Subfield
}

trait MarcOps[Marc <: MARC] {

  implicit val dataField: Marc#DataField => Marc#Field
  implicit val controlField: Marc#ControlField => Marc#Field

  val collectionRecords: Marc#Collection => Seq[Marc#Record]
  val recordControlFields: Marc#Record => Seq[Marc#ControlField]
  val recordDataFields: Marc#Record => Seq[Marc#DataField]
  val dataFieldSubfields: Marc#DataField => Seq[Marc#Subfield]

  val recordLeader: Marc#Record => String
  val fieldTag: Marc#Field => String
  val controlFieldValue: Marc#ControlField => String
  val dataFieldIndicator1: Marc#DataField => Char
  val dataFieldIndicator2: Marc#DataField => Char
  val subfieldCode: Marc#Subfield => Char
  val subfieldValue: Marc#Subfield => String

  implicit case object CollectionRecords   extends MARC.Property[Marc#Collection, Seq[Marc#Record]]   { val property = collectionRecords   }
  implicit case object RecordControlFields extends MARC.Property[Marc#Record, Seq[Marc#ControlField]] { val property = recordControlFields }
  implicit case object RecordDataFields    extends MARC.Property[Marc#Record, Seq[Marc#DataField]]    { val property = recordDataFields    }
  implicit case object DataFieldSubfields  extends MARC.Property[Marc#DataField, Seq[Marc#Subfield]]  { val property = dataFieldSubfields  }

  case object RecordLeader        extends MARC.Property[Marc#Record, String]       { val property = recordLeader        }
  case object FieldTag            extends MARC.Property[Marc#Field, String]        { val property = fieldTag            }
  case object ControlFieldValue   extends MARC.Property[Marc#ControlField, String] { val property = controlFieldValue   }
  case object DataFieldIndicator1 extends MARC.Property[Marc#DataField, Char]      { val property = dataFieldIndicator1 }
  case object DataFieldIndicator2 extends MARC.Property[Marc#DataField, Char]      { val property = dataFieldIndicator2 }
  case object SubfieldCode        extends MARC.Property[Marc#Subfield, Char]       { val property = subfieldCode        }
  case object SubfieldValue       extends MARC.Property[Marc#Subfield, String]     { val property = subfieldValue       }

  case object DataFieldTag    extends MARC.Property[Marc#DataField, String]    { val property = (df: Marc#DataField) => fieldTag(df)    }
  case object ControlFieldTag extends MARC.Property[Marc#ControlField, String] { val property = (df: Marc#ControlField) => fieldTag(df) }
  //  implicit def propConv[P1,P2](v: MARC.Property[P1,String])(implicit conv:P2=>P1)=MARC.Property[P2,String]((p:P2)=>v.property(conv(p)))

  implicit class CollectionExt(c: Marc#Collection) {
    def records = CollectionRecords.property(c)
  }

  implicit class RecordExt(r: Marc#Record) {
    def ldr           = RecordLeader.property(r)
    def controlFields = RecordControlFields.property(r)
    def dataFields    = RecordDataFields.property(r)
  }
  class FieldExt(f: Marc#Field) {
    def tag = FieldTag.property(f)
  }
  implicit class ControlFieldExt(f: Marc#ControlField) extends FieldExt(f) {
    def value = ControlFieldValue.property(f)
  }
  implicit class DataFieldExt(f: Marc#DataField) extends FieldExt(f) {
    def i1        = DataFieldIndicator1.property(f)
    def i2        = DataFieldIndicator2.property(f)
    def subfields = DataFieldSubfields.property(f)
  }
  implicit class SubfieldExt(sf: Marc#Subfield) {
    def code  = SubfieldCode.property(sf)
    def value = SubfieldValue.property(sf)
  }
}
trait MarcConstructorOps[Marc <: MARC] {
  val newRecord: (String, Seq[Marc#ControlField], Seq[Marc#DataField]) => Marc#Record
  val newControlField: (String, String) => Marc#ControlField
  val newDataField: (String, Char, Char, Seq[Marc#Subfield]) => Marc#DataField
  val newSubfield: (Char, String) => Marc#Subfield
}

class MarcTypesDef[Marc <: MARC] {
  type Collection   = Marc#Collection
  type Record       = Marc#Record
  type Field        = Marc#Field
  type ControlField = Marc#ControlField
  type DataField    = Marc#DataField
  type Subfield     = Marc#Subfield
}
trait MarcTypes[Marc <: MARC] {
  val marcTypes = new MarcTypesDef[Marc]
}
