package ch.datuman.marc4s
package util

class MarcUtility[Marc <: MARC](implicit ops: MarcOps[Marc] with MarcConstructorOps[Marc]) {
  import ops._
  def textToSubfield(value: String, code: Option[Char] = None) =
    (value, code) match {
      case (v, _) if v.trim.isEmpty                 => None
      case (v, Some(c))                             => Some(newSubfield(c, v.trim))
      case (v, None) if v.trim.drop(1).trim.isEmpty => None
      case (v, None)                                => Some(newSubfield(v.trim.head, v.trim.drop(1).trim))
    }

  def textToSubfields(text: String, initialDefault: Char = 'a', subfieldSeparatorRegex: String = "\\$\\$") =
    text.split(subfieldSeparatorRegex).toList match {
      case Nil => Nil
      case parts if parts.head.trim.isEmpty =>
        parts.tail.flatMap(p => textToSubfield(p))
      case parts =>
        textToSubfield(parts.head, Some(initialDefault)).toList ++ parts.tail.flatMap(p => textToSubfield(p))
    }

  def prettyString(rec: Marc#Record, fieldSeparator: String = "\n", valueSeparator: String = "\t") =
    s"LDR$valueSeparator $valueSeparator $valueSeparator${rec.ldr}$fieldSeparator" +
      rec.controlFields
        .map(f => s"${f.tag}$valueSeparator $valueSeparator $valueSeparator${f.value}")
        .mkString(fieldSeparator) + fieldSeparator +
      rec.dataFields
        .map(
          f =>
            s"${f.tag}$valueSeparator${f.i1}$valueSeparator${f.i2}$valueSeparator${f.subfields.map(sf => "$$" + s"${sf.code} ${sf.value}").mkString(" ")}"
        )
        .mkString(fieldSeparator)

  /**
    * Parse textual MARC into Marc4s elements
    *
    * WARNING: current parsing method has several limitations:
    * - could not handle mutli-line subfields values
    * - the indicator values should be distinct from valueSeparator (we cannot use <space> to indicate both empty char and separation between tag/indicator/subfields)
    *
    * @param recordSeparatorRegex Regex to separate multiple records (default \n\\-+\n)
    * @param fieldSeparator string separating fields in a record (default \n)
    * @param valueSeparator string separating fields properties (default \t)
    * @param ldrTagRegex
    * @param hasControlFieldIndicators
    * @param emptyIndicator Char used to indicate empty indicator(default to ' ' but if " " is used as valueSeparator then a different Char should be used to indicate empty indicator, e.g. '#' or '.')
    * @param hasIndicatorSeparator flag to indicate wether a separator is used between indicators (true) or if the two Char values are adjacent (false)
    * @param subfieldPrefixRegex prefix used before subfield code (default to "\\$\\$")
    */
  class MarcTxtCodec(
      recordSeparatorRegex: String = "\n\\-+\n",
      fieldSeparator: String = "\n",
      valueSeparator: String = "\t",
      ldrTagRegex: String = "(LDR|ldr|000)",
      hasControlFieldIndicators: Boolean = true,
      emptyIndicator: Char = ' ',
      hasIndicatorSeparator: Boolean = true,
      subfieldPrefixRegex: String = "\\$\\$",
      enforceLdrPresence: Boolean = true
  ) {

    def writeRecord(rec: Marc#Record) =
      (
        List(List(ldrTagRegex.filter(_.isLetterOrDigit).take(3), rec.ldr).mkString(valueSeparator)) ++
          rec.controlFields.map(writeControlField) ++
          rec.dataFields.map(writeDataField)
      ).mkString(fieldSeparator)

    private def writeControlField(cf: Marc#ControlField) =
      if (hasControlFieldIndicators && hasIndicatorSeparator)
        List(cf.tag, "", "", cf.value).mkString(valueSeparator)
      else if (hasControlFieldIndicators)
        List(cf.tag, "", cf.value).mkString(valueSeparator)
      else
        List(cf.tag, cf.value).mkString(valueSeparator)
    private def readControlField(cfLine: String) = cfLine.split(valueSeparator) match {
      case res =>
        newControlField(
          res.head,
          res
            .drop(1 + (if (hasControlFieldIndicators && hasIndicatorSeparator) 2 else if (hasControlFieldIndicators) 1 else 0))
            .mkString(valueSeparator)
        )
    }
    private def writeDataField(df: Marc#DataField) =
      (
        hasIndicatorSeparator,
        (
          df.i1.toString.replace(' ', emptyIndicator),
          df.i2.toString.replace(' ', emptyIndicator),
          df.subfields.map(sf => subfieldPrefixRegex.replace("\\", "") + sf.code + " " + sf.value)
        )
      ) match {
        case (false, (i1, i2, sfs)) => (List(df.tag, i1 + i2) ++ sfs).mkString(valueSeparator)
        case (true, (i1, i2, sfs))  => (List(df.tag, i1, i2) ++ sfs).mkString(valueSeparator)
      }

    private def readDataField(dfLine: String) =
      dfLine.split(valueSeparator) match {
        case res =>
          newDataField(
            res(0),
            res(1).replace(emptyIndicator, ' ').headOption.getOrElse(' '),
            (if (hasIndicatorSeparator) res(2) else res(1).drop(1)).replace(emptyIndicator, ' ').headOption.getOrElse(' '),
            textToSubfields(
              res.drop(if (hasIndicatorSeparator) 3 else 2).mkString(valueSeparator),
              subfieldSeparatorRegex = subfieldPrefixRegex
            )
          )
      }
    def readDataFields(marcTxt: String) =
      marcTxt
        .split(fieldSeparator)
        .toList
        .filterNot(_.trim.isEmpty)
        .map(readDataField)

    def readRecords(marcTxt: String) =
      marcTxt
        .split(recordSeparatorRegex)
        .toList
        .filterNot(_.trim.isEmpty)
        // .map{v=>println("read " + v);v}
        .map(readRecord)
    def readRecord(marcTxt: String) =
      marcTxt
        .split(fieldSeparator)
        .filterNot(_.trim.isEmpty)
        .span(l => l.take(2) == "00" || l.take(3).toUpperCase.matches(ldrTagRegex)) match {
        case (ctrlLines, dfLines) =>
          newRecord(
            (if (enforceLdrPresence) ctrlLines.head else ctrlLines.headOption.getOrElse(""))
              .split(valueSeparator)
              .drop(1)
              .mkString(valueSeparator),
            ctrlLines.toList.tail.map(readControlField),
            dfLines.toList.map(readDataField)
          )

      }
  }

}
