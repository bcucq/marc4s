import sbtcrossproject.CrossType
import sbtcrossproject.CrossPlugin.autoImport.crossProject


organization in ThisBuild := "ch.datuman"

name := "marc4s"

version in ThisBuild := "1.5.0-SNAPSHOT"


val scala212VersionNum = if (scalaJSVersion.startsWith("0.6.")) "2.12.13" else "2.12.18"

val playJsonVersion = if (scalaJSVersion.startsWith("0.6.")) "2.7.4" else "2.9.4"

val scala213VersionNum = "2.13.12"

crossScalaVersions in ThisBuild := Seq(scala212VersionNum, scala213VersionNum)

val scalaXmlVersion = "2.2.0"
val scalaTestVersion =  "3.2.3"
val scalaJsStubsVersion = "1.1.0"

scalaVersion in ThisBuild := scala212VersionNum

publishTo in ThisBuild := Some("datuman-maven-repo" at "http://178.62.90.239:8081/artifactory/datuman-public/")
resolvers in ThisBuild += "datuman-maven-repo" at "http://178.62.90.239:8081/artifactory/datuman-public/"
credentials in ThisBuild  += Credentials(Path.userHome / ".sbt" / ".datuman-credentials")
credentials in ThisBuild ++= sys.env
  .get("DATUMAN_CI_PASSWORD")
  .map(pass => Credentials("Artifactory Realm", "178.62.90.239", "datuman-ci", pass))
  .toSeq


lazy val crossType = CrossType.Full



lazy val marc4s = (crossProject(JVMPlatform, JSPlatform).crossType(CrossType.Full) in file("."))
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.play" %%% "play-json" % playJsonVersion,
      "org.scalatest" %%% "scalatest" % scalaTestVersion % "test"
    )
  )
  .jvmSettings(
    libraryDependencies ++= Seq(
      "org.scala-js" %% "scalajs-stubs" % scalaJsStubsVersion,
      "org.scala-lang.modules" %% "scala-xml" % scalaXmlVersion
    )
  )


