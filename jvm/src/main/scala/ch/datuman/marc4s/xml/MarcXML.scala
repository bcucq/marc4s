package ch.datuman.marc4s
package xml

import scala.xml._
import scala.xml.transform._
import ch.datuman.marc4s.MARC
import ch.datuman.marc4s.MarcOps
import ch.datuman.marc4s.MarcTypes
import scala.xml.NodeSeq.seqToNodeSeq

trait MarcXML extends MARC {
  type Collection   = NodeSeq
  type Record       = Elem
  type Field        = Elem
  type ControlField = Elem
  type DataField    = Elem
  type Subfield     = Elem
}
object MarcXMLOps extends MarcOps[MarcXML] with MarcTypes[MarcXML] {
  import marcTypes._
  implicit val dataField    = (f: DataField) => f
  implicit val controlField = (f: ControlField) => f

  val collectionRecords = ((c: Collection) => c.collect { case e: Elem => e }: Seq[Record])
  val recordControlFields = { (rec: Record) =>
    (rec \ "controlfield").collect { case e: Elem => e }: Seq[ControlField]
  }
  val recordDataFields   = ((rec: Record) => (rec \ "datafield").collect { case e: Elem   => e }: Seq[DataField])
  val dataFieldSubfields = ((rec: DataField) => (rec \ "subfield").collect { case e: Elem => e }: Seq[Subfield])

  val recordLeader        = ((r: Record) => (r \ "leader").headOption.map(_.child.text).getOrElse(""))
  val fieldTag            = ((_: Field) \@ "tag")
  val controlFieldValue   = ((_: ControlField).child.text)
  val dataFieldIndicator1 = ((df: DataField) => (df \@ "ind1").headOption.getOrElse(' '))
  val dataFieldIndicator2 = ((df: DataField) => (df \@ "ind2").headOption.getOrElse(' '))
  val subfieldCode        = ((sf: Subfield) => (sf \@ "code").headOption.getOrElse(' '))
  val subfieldValue       = ((_: Subfield).child.text)
}

object MarcXMLConstructorOps extends MarcConstructorOps[MarcXML] with MarcTypes[MarcXML] {
  import marcTypes._
  val newRecord: (String, Seq[ControlField], Seq[DataField]) => Record = { (ldr: String, cfs: Seq[ControlField], dfs: Seq[DataField]) => //(values match {case (ldr,cfs,dfs) =>
    val ldrElem =
      if (ldr.isEmpty) NodeSeq.Empty
      else {
        <leader>{ldr}</leader>
      }
    <record>
	{ldrElem}
  {cfs}
  {dfs}
</record>
  } // : Record}
  val newControlField = { (tag: String, data: String) => //values match {case (tag,data) =>
    <controlfield tag={tag}>{data}</controlfield>
  }
  val newDataField = { (tag: String, i1: Char, i2: Char, sfs: Seq[Subfield]) => //values match {case (tag,i1,i2,sfs) =>
    <datafield ind1={i1.toString} ind2={i2.toString} tag={tag}>
{sfs}
</datafield>
  }
  val newSubfield = { (c: Char, v: String) => //values match {case (c,v) =>
    <subfield code={c.toString}>{v}</subfield>
  }
}
