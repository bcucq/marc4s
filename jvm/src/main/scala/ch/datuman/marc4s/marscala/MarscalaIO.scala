package ch.datuman.marc4s
package marscala

object MarscalaReader extends Reader[Marscala] {
  val marcXml = xml.MarcXMLOps
  def read(n: scala.xml.Elem, leader: String) = {
    Marscala.Record(
      leader,
      marcXml.RecordControlFields
        .property(n)
        .map(cf => Marscala.ControlField(marcXml.FieldTag.property(cf), marcXml.ControlFieldValue.property(cf))),
      marcXml.RecordDataFields
        .property(n)
        .map(
          df =>
            Marscala.DataField(
              marcXml.FieldTag.property(df),
              marcXml.DataFieldIndicator1.property(df),
              marcXml.DataFieldIndicator2.property(df),
              marcXml.DataFieldSubfields
                .property(df)
                .map(sf => Marscala.Subfield(marcXml.SubfieldCode.property(sf), marcXml.SubfieldValue.property(sf)))
            )
        )
    )
  }
  def read(n: scala.xml.Elem) = {
    read(n, marcXml.RecordLeader.property(n))
  }

}

object MarscalaWriter extends Writer[Marscala] {
  val marcXml     = xml.MarcXMLOps
  val marcXmlCons = xml.MarcXMLConstructorOps
  def write(rec: Marscala.Record, leader: String) = {
    marcXmlCons.newRecord(
      leader,
      rec.controlFields.map(cf => marcXmlCons.newControlField(cf.tag, cf.data)),
      rec.dataFields.map(
        df => marcXmlCons.newDataField(df.tag, df.i1, df.i2, df.subfields.map(sf => marcXmlCons.newSubfield(sf.code, sf.data)))
      )
    )
  }
  def write(n: Marscala.Record) = {
    write(n, n.ldr)
  }

}
