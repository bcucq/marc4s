package ch.datuman.marc4s

trait Reader[Marc <: MARC] {

  def read(n: scala.xml.Elem): Marc#Record
}
trait Writer[Marc <: MARC] {
  def write(rec: Marc#Record): scala.xml.Elem
}
/**




/**
  * Represents a record label in a MARC record.
  *
  * @author Bas Peters
  */
case class Leader (
    /** The logical record length (Position 0-4). */
    recordLength : Int,
    /** The record status (Position 5). */
    recordStatus : Char,

    /** Type of record (Position 6). */
    typeOfRecord : Char,

    /** Implementation defined (Position 7-8). */
    implDefined1 : Array[Char],

    /** Character coding scheme (Position 9). */
    charCodingScheme : Char,

    /** The indicator count (Position 10). */
    indicatorCount : Char,

    /** The subfield code length (Position 11). */
    subfieldCodeLength : Char,

    /** The base address of data (Position 12-16). */
    baseAddressOfData : Int,

    /** Implementation defined (Position 17-19) */
    implDefined2 : Array[Char],

    /** Entry map (Position 20-23). */
    entryMap : Array[Char]
  ) {

    def formatTruncated(v:Int,numDigits:Int=5)=
      ("%0" + numDigits + "d").format(v) match {
  	    case r if r.size > numDigits => "9" * numDigits
        case r => r
      }
    def toString()=
      formatTruncated(recordLength)+ recordStatus + typeOfRecord +
      implDefined1.mkString + charCodingScheme + indicatorCount + subfieldCodeLength +
      formatTruncated(baseAddressOfData) + implDefined2.mkString + entryMap.mkString
}





trait BinaryStreamReader[Marc <: MARC] {

  def encoding:Option[String]

  def read(n:scala.xml.Elem): Marc#Record

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;


    def parseLeader(recordLength : Int,leaderData: Array[Byte])={
      val isr = new InputStreamReader(new ByteArrayInputStream(leaderData), "ISO-8859-1");
      val tmp = Array.fill[Char](5)(' ')
      isr.read(tmp);
      def readNext=isr.read.toChar
      // Skip over bytes for record length, If we get here, its already been
      // computed.
      Leader(
        recordLength=recordLength,
        recordStatus= readNext,
        typeOfRecord= readNext,
        implDefined1= Array[Char](readNext,readNext),
        charCodingScheme= readNext,
        indicatorCount = readNext,
        subfieldCodeLength = readNext,
        baseAddressOfData = Array[Char](readNext,readNext,readNext,readNext,readNext).mkString.toInt,
        implDefined2 = Array[Char](readNext,readNext,readNext),
        entryMap = Array[Char](readNext,readNext,readNext,readNext)
      )
    }


    def parseRecord(aByteArray : Array[Byte], recordBuf: Array[Byte],recordLength:Int) {

        val byteArray = aByteArray;
        val ldr = parseLeader(recordLength, byteArray);
        val directoryLength = ldr.baseAddressOfData - (24 + 1);
        val enc = encoding.getOrElse(if (ldr.charCodingScheme == ' ') "ISO-8859-1" else "UTF8")

        if (directoryLength % 12 != 0) {
            throw new Exception("invalid marc directory");
        }

        val inputrec = new DataInputStream(new ByteArrayInputStream(recordBuf));
        val size = directoryLength / 12;

        final String[] tags = new String[size];
        final int[] lengths = new int[size];
        final int[] starts = new int[size];
        final HashMap<Integer, Integer> unsortedStartIndex = new HashMap<Integer, Integer>();

        final byte[] tag = new byte[3];
        final byte[] length = new byte[4];
        final byte[] start = new byte[5];

        String tmp;

        try {
            for (int i = 0; i < size; i++) {
                inputrec.readFully(tag);
                tmp = new String(tag);
                tags[i] = tmp;

                inputrec.readFully(length);
                tmp = new String(length);
                lengths[i] = Integer.parseInt(tmp);

                inputrec.readFully(start);

                tmp = new String(start);
                starts[i] = Integer.parseInt(tmp);
                unsortedStartIndex.put(starts[i], i);
            }

            // Sort starting character positions
            Arrays.sort(starts);

            if (inputrec.read() != Constants.FT) {
                throw new MarcException("expected field terminator at end of directory");
            }

            int i = 0;
            for (int s = 0; s < size; s++) {
                i = unsortedStartIndex.get(starts[s]).intValue();

                getFieldLength(inputrec);

                if (Verifier.isControlField(tags[i])) {
                    byteArray = new byte[lengths[i] - 1];
                    inputrec.readFully(byteArray);

                    if (inputrec.read() != Constants.FT) {
                        throw new MarcException("expected field terminator at end of field");
                    }

                    final ControlField field = factory.newControlField();
                    field.setTag(tags[i]);
                    field.setData(getDataAsString(byteArray));
                    record.addVariableField(field);
                } else {
                    byteArray = new byte[lengths[i]];
                    inputrec.readFully(byteArray);

                    try {
                        record.addVariableField(parseDataField(tags[i], byteArray));
                    } catch (final IOException e) {
                        throw new MarcException("error parsing data field for tag: " + tags[i] + " with data: " +
                                new String(byteArray), e);
                    }
                }
            }

            if (inputrec.read() != Constants.RT) {
                throw new MarcException("expected record terminator");
            }
        } catch (final IOException e) {
            throw new MarcException("an error occured reading input", e);
        }
    }

    private DataField parseDataField(final String tag, final byte[] field) throws IOException {
        final ByteArrayInputStream bais = new ByteArrayInputStream(field);
        final char ind1 = (char) bais.read();
        final char ind2 = (char) bais.read();

        final DataField dataField = factory.newDataField();
        dataField.setTag(tag);
        dataField.setIndicator1(ind1);
        dataField.setIndicator2(ind2);

        int code;
        int size;
        int readByte;
        byte[] data;
        Subfield subfield;
        while (true) {
            readByte = bais.read();
            if (readByte < 0) {
                break;
            }
            switch (readByte) {
                case Constants.US:
                    code = bais.read();
                    if (code < 0) {
                        throw new IOException("unexpected end of data field");
                    }
                    if (code == Constants.FT) {
                        break;
                    }
                    size = getSubfieldLength(bais);
                    data = new byte[size];
                    bais.read(data);
                    subfield = factory.newSubfield();
                    subfield.setCode((char) code);
                    subfield.setData(getDataAsString(data));
                    dataField.addSubfield(subfield);
                    break;
                case Constants.FT:
                    break;
            }
        }
        return dataField;
    }

    private int getFieldLength(final DataInputStream bais) throws IOException {
        bais.mark(9999);
        int bytesRead = 0;
        while (true) {
            switch (bais.read()) {
                case Constants.FT:
                    bais.reset();
                    return bytesRead;
                case -1:
                    bais.reset();
                    throw new IOException("Field not terminated");
                case Constants.US:
                default:
                    bytesRead++;
            }
        }
    }

    private int getSubfieldLength(final ByteArrayInputStream bais) throws IOException {
        bais.mark(9999);
        int bytesRead = 0;
        while (true) {
            switch (bais.read()) {
                case Constants.US:
                case Constants.FT:
                    bais.reset();
                    return bytesRead;
                case -1:
                    bais.reset();
                    throw new IOException("subfield not terminated");
                default:
                    bytesRead++;
            }
        }
    }

    private int parseRecordLength(final byte[] leaderData) throws IOException {
        final InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(leaderData), "ISO-8859-1");
        int length = -1;
        final char[] tmp = new char[5];
        isr.read(tmp);
        try {
            length = Integer.parseInt(new String(tmp));
        } catch (final NumberFormatException e) {
            throw new MarcException("unable to parse record length", e);
        }
        return length;
    }

    private void parseLeader(final Leader ldr, final byte[] leaderData) throws IOException {
        final InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(leaderData), "ISO-8859-1");
        char[] tmp = new char[5];
        isr.read(tmp);
        // Skip over bytes for record length, If we get here, its already been
        // computed.
        ldr.setRecordStatus((char) isr.read());
        ldr.setTypeOfRecord((char) isr.read());
        tmp = new char[2];
        isr.read(tmp);
        ldr.setImplDefined1(tmp);
        ldr.setCharCodingScheme((char) isr.read());
        final char indicatorCount = (char) isr.read();
        final char subfieldCodeLength = (char) isr.read();
        final char baseAddr[] = new char[5];
        isr.read(baseAddr);
        tmp = new char[3];
        isr.read(tmp);
        ldr.setImplDefined2(tmp);
        tmp = new char[4];
        isr.read(tmp);
        ldr.setEntryMap(tmp);
        isr.close();
        try {
            ldr.setIndicatorCount(Integer.parseInt(String.valueOf(indicatorCount)));
        } catch (final NumberFormatException e) {
            throw new MarcException("unable to parse indicator count", e);
        }
        try {
            ldr.setSubfieldCodeLength(Integer.parseInt(String.valueOf(subfieldCodeLength)));
        } catch (final NumberFormatException e) {
            throw new MarcException("unable to parse subfield code length", e);
        }
        try {
            ldr.setBaseAddressOfData(Integer.parseInt(new String(baseAddr)));
        } catch (final NumberFormatException e) {
            throw new MarcException("unable to parse base address of data", e);
        }

    }

    private String getDataAsString(final byte[] bytes) {
        String dataElement = null;
        if (encoding.equals("UTF-8") || encoding.equals("UTF8")) {
            try {
                dataElement = new String(bytes, "UTF8");
            } catch (final UnsupportedEncodingException e) {
                throw new MarcException("unsupported encoding", e);
            }
        } else if (encoding.equals("MARC-8") || encoding.equals("MARC8")) {
            if (converterAnsel == null) {
                converterAnsel = new AnselToUnicode();
            }
            dataElement = converterAnsel.convert(bytes);
        } else if (encoding.equals("ISO-8859-1") || encoding.equals("ISO8859_1") || encoding.equals("ISO_8859_1")) {
            try {
                dataElement = new String(bytes, "ISO-8859-1");
            } catch (final UnsupportedEncodingException e) {
                throw new MarcException("unsupported encoding", e);
            }
        } else if (override) {
            try {
                dataElement = new String(bytes, encoding);
            } catch (final UnsupportedEncodingException e) {
                throw new MarcException("unsupported encoding", e);
            }
        }
        return dataElement;
    }


}
trait BinaryWriter[Marc <: MARC] {
  def write(rec:Marc#Record): scala.xml.Elem
}



  */
