package ch.datuman.marc4s

object SampleRecord {
  val defaultSampleRecord =
    <record>
	<leader>00903cam a2200253 a 4500</leader>
	<controlfield tag="001">991003632619702852</controlfield>
	<controlfield tag="005">20160902141741.0</controlfield>
	<controlfield tag="008">150831s2004fr |||||| ||||00|| |fred</controlfield>
	<datafield ind1=" " ind2=" " tag="020">
		<subfield code="a">2070743993</subfield>
		<subfield code="q">broché</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="035">
		<subfield code="a">(RNV)008241092-41bculausa_network</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="035">
		<subfield code="a">(RERO)008241092</subfield>
		<subfield code="9">ExL</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="035">
		<subfield code="a">R008241092</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="035">
		<subfield code="a">(EXLNZ-41BCULAUSA_NETWORK)991018171489702851</subfield>
	</datafield>
	<datafield ind1=" " ind2="9" tag="039">
		<subfield code="a">201509011808</subfield>
		<subfield code="b">8123</subfield>
		<subfield code="c">201509011807</subfield>
		<subfield code="d">8123</subfield>
		<subfield code="y">201508311552</subfield>
		<subfield code="z">8007</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="040">
		<subfield code="a">RERO vdbcul</subfield>
	</datafield>
	<datafield ind1=" " ind2="7" tag="072">
		<subfield code="a">s1fr</subfield>
		<subfield code="2">rero</subfield>
	</datafield>
	<datafield ind1="1" ind2=" " tag="100">
		<subfield code="a">Jaccottet, Philippe</subfield>
	</datafield>
	<datafield ind1="1" ind2=" " tag="110">
		<subfield code="a">Jaccottet, Philippe</subfield>
	</datafield>
	<datafield ind1="1" ind2="3" tag="245">
		<subfield code="b">La seconde semaison :</subfield>
		<subfield code="b">carnets, 1980-1994 /</subfield>
		<subfield code="c">Philippe Jacottet</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="260">
		<subfield code="a">[Paris] :</subfield>
		<subfield code="b">Gallimard,</subfield>
		<subfield code="c">2004</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="300">
		<subfield code="a">231 p. ;</subfield>
		<subfield code="c">21 cm</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="500">
		<subfield code="a">Les carnets 1980-1984 ont d'abord paru sous le titre: "Autres journées"</subfield>
	</datafield>
	<datafield ind1="4" ind2=" " tag="510">
		<subfield code="a">La Revue de Belles-Lettres, 1997, no 1-2, p.105-106 (Gérard Bocholier)</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="900">
		<subfield code="a">nwdorfrancais</subfield>
		<subfield code="d">TEST ERREUR</subfield>
		<subfield code="x">0</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="905">
		<subfield code="a">nwdorfrancais</subfield>
		<subfield code="b">1508</subfield>
		<subfield code="c">0</subfield>
		<subfield code="9">LOCAL</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="972">
		<subfield code="a">vdbcul</subfield>
	</datafield>
	<datafield ind1=" " ind2=" " tag="985">
		<subfield code="2">BCUDfr1</subfield>
		<subfield code="a">840"19"JAC7Sec</subfield>
		<subfield code="b">Test erreur</subfield>
		<subfield code="9">LOCAL</subfield>
	</datafield>
</record>

}
