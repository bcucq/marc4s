package ch.datuman.marc4s

// import org.scalatest.{Matchers, FlatSpec}

import marscala.Marscala
import marscala.{MarscalaWriter, MarscalaReader}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MarscalaXmlCodec extends AnyFlatSpec with Matchers {

  "MarscalaIO" should "convert from and to xml without any change" in {
    val sample = (SampleRecord.defaultSampleRecord)
    scala.xml.Utility.trim(MarscalaWriter.write(MarscalaReader.read(sample))) should equal(scala.xml.Utility.trim(sample))
  }
}
